#!/bin/bash
FILENAME="./vars.tf"


func1()
{

    echo "Command Substitution:"
    echo "1.-  7 + 7 = `expr 7 + 7` "
}

checkfile()
{
  if test -f "$FILENAME"; then
    rm "$FILENAME"
fi
}

main()
{
  checkfile
  func1
}

main
